var express = require('express');
var router = express.Router();
var Mailgun = require('mailgun').Mailgun;
var mg = new Mailgun('key-961dace1783005b8035b8a0005cded4a');
var path = require('path');
var qs = require('querystring');
var Q = require("q");
var async = require('async');
var bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
var colors = require('colors');
var cors = require('cors');
var express = require('express');
var logger = require('morgan');
var jwt = require('jwt-simple');
var moment = require('moment');
var mongoose = require('mongoose');
var request = require('request');
/*var userSchema = require('./../db/user_schema');*/
/*var User = require('./../db/connector');*/
var config = require('./../config');
/*var ensureAuthenticated = require('./../security/auth_filter');*/


var userSchema = new mongoose.Schema({
    email: {type: String, unique: true, lowercase: true},
    password: {type: String, select: false},
    displayName: String,
    picture: String,
    facebook: String,
    foursquare: String,
    google: String,
    github: String,
    linkedin: String,
    live: String,
    yahoo: String,
    twitter: String,
    role: {type: Number, value: 0},
    emailCount: {type: Number, value: 0},
    joinedCount: {type: Number, value: 0},
    uid: {type: Number, value: 0}
});

userSchema.pre('save', function (next) {
    var user = this;
    if (!user.isModified('password')) {
        return next();
    }
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, function (err, hash) {
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function (password, done) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
        done(err, isMatch);
    });
};

var User = mongoose.model('User', userSchema);

mongoose.connect(config.MONGO_URI);
mongoose.connection.on('error', function (err) {
    console.log('Error: Could not connect to MongoDB. Did you forget to run `mongod`?'.red);
});


/*
 |--------------------------------------------------------------------------
 | Login Required Middleware
 |--------------------------------------------------------------------------
 */
function ensureAuthenticated(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(401).send({message: 'Please make sure your request has an Authorization header'});
    }
    var token = req.headers.authorization.split(' ')[1];

    var payload = null;
    try {
        payload = jwt.decode(token, config.TOKEN_SECRET);
    }
    catch (err) {
        return res.status(401).send({message: err.message});
    }

    if (payload.exp <= moment().unix()) {
        return res.status(401).send({message: 'Token has expired'});
    }
    req.user = payload.sub;
    next();
}

/*
 |--------------------------------------------------------------------------
 | Generate JSON Web Token
 |--------------------------------------------------------------------------
 */
function createJWT(user) {
    var payload = {
        sub: user._id,
        iat: moment().unix(),
        exp: moment().add(14, 'days').unix(),
        role: user.role
    };
    return jwt.encode(payload, config.TOKEN_SECRET);
}

/*
 |--------------------------------------------------------------------------
 | GET /api/me
 |--------------------------------------------------------------------------
 */
router.get('/api/me', ensureAuthenticated, function (req, res, next) {
    User.findById(req.user, function (err, user) {
        res.send(user);
    });
});

/*
 |--------------------------------------------------------------------------
 | PUT /api/me
 |--------------------------------------------------------------------------
 */
router.put('/api/me', ensureAuthenticated, function (req, res, next) {
    User.findById(req.user, function (err, user) {
        if (!user) {
            return res.status(400).send({message: 'User not found'});
        }
        user.displayName = req.body.displayName || user.displayName;
        user.email = req.body.email || user.email;
        user.save(function (err) {
            res.status(200).end();
        });
    });
});


/*
 |--------------------------------------------------------------------------
 | Log in with Email
 |--------------------------------------------------------------------------
 */
router.post('/login', function (req, res) {
    User.findOne({email: req.body.username}, '+password', function (err, user) {

        if (!user) {
            return res.status(401).send({message: 'Wrong email and/or password'});
        }
        user.comparePassword(req.body.password, function (err, isMatch) {
            if (!isMatch) {
                return res.status(401).send({message: 'Wrong email and/or password'});
            }
            res.send({token: createJWT(user)});
        });
    });
});

/*
 |--------------------------------------------------------------------------
 | Create Email and Password Account
 |--------------------------------------------------------------------------
 */

router.post('/signup', function (req, res) {
    var UID = Math.floor(100000 + Math.random() * 900000);
    User.findOne({email: req.body.username}, function (err, existingUser) {
        if (existingUser) {
            return res.status(409).send({message: 'Email is already taken'});
        }
        var user = new User({
            /* displayName: req.body.username,*/
            email: req.body.username,
            password: req.body.password,
            role: '0',
            emailCount: "0",
            uid: UID,
            joinedCount: "0"
        });
        user.save(function () {
            res.send({token: createJWT(user)});
        });
    });
});
/*
 |--------------------------------------------------------------------------
 | send Email
 |--------------------------------------------------------------------------
 */
router.post('/send', ensureAuthenticated, function (req, res, next) {

    /* var User = dbConnector.User;*/
    var send = {
        email: req.body.email,
        message: req.body.message

    };


    var uuid = function (userID) {
        var deffered = Q.defer();
        User.findById(userID, function (err, user) {

            if (err) {
                deffered.reject(err)
            }
            else {
                var uid = user.uid
                deffered.resolve(uid);
            }
        });
        return deffered.promise
    }
    var userID = req.user
    var uuidPromise = uuid(userID);
    uuidPromise.then(function (result) {
        mg.sendText('pcnfernando@gmail.com',
            [send.email],
            ['Join with EduLink!'],
            [send.message + 'Provide this code when you join the institute and redeem the gift' + '  ' + result],
            {'X-Campaign-Id': 'something'},
            function (err) {
                if (!err) {
                    User.findById(req.user, function (err, user) {
                        user.modified = user.emailCount++
                        user.save(function (err) {
                            if (err)
                                console.log('error')
                            else
                                console.log('success')
                        });
                    });

                }
                err && console.log(err)
            }
        )
        ;

    }, function (err) {

    });


    res.send('respond with a resource');


});
/*
 |--------------------------------------------------------------------------
 | Email count
 |--------------------------------------------------------------------------
 */
router.get('/emailCount', ensureAuthenticated, function (req, res, next) {
    User.findById(req.user, function (err, user) {
        var email_count = user.emailCount
        return res.send({email_count: email_count});
    });
});
/*
 |--------------------------------------------------------------------------
 |joined count
 |--------------------------------------------------------------------------
 */
router.post('/joinedCount', ensureAuthenticated, function (req, res, next) {
    User.findOne({uid: req.body.uid}, function (err, user) {

        user.modified = user.joinedCount++;
        user.save(function (err) {
                if (!err) {
                    console.log('joined count increased');
                }
                else {
                    console.log('joined count error');
                }
                err && console.log(err);
            }
        )
    });
    res.send('respond with a resource');

});
/*
 |--------------------------------------------------------------------------
 | get joined count
 |--------------------------------------------------------------------------
 */
router.get('/getJoinedCount', ensureAuthenticated, function (req, res, next) {
    User.findById(req.user, function (err, user) {
        console.log(user.joinedCount);
        var getJoinedCount = user.joinedCount
        return res.send({getJoinedCount: getJoinedCount});
    });
});

/*
 |--------------------------------------------------------------------------
 | total emails sent
 |--------------------------------------------------------------------------
 */
router.get('/totalEmailCount', ensureAuthenticated, function (req, res, next) {
    User.find(function (err, user) {
        if (err) return next(err);

        var total = 0;
        for (i = 0; i < user.length; i++) {
            total = total + user[i].emailCount;
        }
    });

    return res.send({total: total});

});


module.exports = router;



